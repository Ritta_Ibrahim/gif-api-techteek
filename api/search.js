const axios = require('axios');
require('dotenv').config()

let url = process.env.GIF_ENDPOINT;

/* Add request query params here  */
let params = { key: process.env.GIF_KEY, media_filter: "gif", limit: 12 }

module.exports = {
    getGif: async function (q) {

        let url = process.env.GIF_ENDPOINT;
        url += "?q=" + q;

        Object.entries(params).forEach(([key, value]) => {
            url += "&" + key + "=" + value
        })
        console.log(url);
        let res = await axios.get(url).catch(err => {
            console.log('Error: ', err.message);
        });

        return res.data;
    }
};