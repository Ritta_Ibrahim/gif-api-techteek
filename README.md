# gif-api-techteek



## Getting started

To run the application please follow these steps:

- [ ] Clone the repo to your local.
- [ ] run the following commands inside the project folder:

```
npm install
node app.js
```
Now you can user the application to search for gif using Tenor providor
## Notes

- [ ] Note that you have to run this app while testing the Laravel project.
- [ ] This is my first time using Nodejs ^_^
