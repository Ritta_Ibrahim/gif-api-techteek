module.exports = {
    getUrls: function (data) {
        urls = [];

        Object.values(data.results).forEach(obj => {
            urls.push(obj.media_formats.gif.url)
        });
        
        return urls;
    }
}