const { getGif } = require('./api/search');
const { getUrls } = require("./helpers/resultsHelper");
const cors = require('cors');

var express = require("express");

var app = express();

app.use(cors({
    origin: '*',
    methods: ['GET']
}));

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.get("/api/gifs",
    async (req, res) => {
        results = await getGif(req.query.q);
        urls = getUrls(results);
        res.json(urls);
    }
);